<?php

namespace App\Controller;

use App\Service\OrderService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends FOSRestController
{
    
    private $orderService;
    
    public function __construct(OrderService $orderService){
        $this->orderService = $orderService;
    }
    
    /**
     * @Route(
     *     "/v1/orders",
     *     name="orders",
     *     methods={"GET"},
     * )
     *
     */
    public function getOrders()
    {
        $produtos = $this->orderService->getOrders();
        return $this->json($produtos);
    }
    
    /**
     * @Route(
     *     "/v1/orders/{id}",
     *     name="order",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     */
    public function getOrder($id)
    {
        $order = $this->orderService->getOrder($id);
        return $this->json($order);
    }
    
    /**
     * @Route(
     *     "/v1/orders",
     *     name="saveOrders",
     *     methods={"POST"}
     * )
     *
     */
    public function saveOrders(Request $request)
    {
        $order = json_decode($request->getContent(), true);
        $id = $this->orderService->saveOrders($order);
        
        if ($id) {
            return $this->json('Produto criado com sucesso');
        }
       
    }
    
    /**
     * @Route(
     *     "/v1/orders",
     *     name="updateOrder",
     *     methods={"PUT"}
     * )
     *
     */
    public function updateOrders(Request $request)
    {
        $order = json_decode($request->getContent(), true);
        $id = $this->orderService->updateOrders($order);
        
        if ($id) {
            return $this->json('Produto criado com sucesso!');
        }
        
    }
}
