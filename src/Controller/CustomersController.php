<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Customer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\CustomerService;
use FOS\RestBundle\Controller\FOSRestController;

class CustomersController extends FOSRestController
{
    private $customerService;
    
    public function __construct(CustomerService $customerService){
        $this->customerService = $customerService;
    }
    
    /**
     * @Route(
     *     "/v1/customers",
     *     name="customers",
     *     methods={"GET"},
     * )
     * 
     * @return JsonResponse
     */
    public function geCustomers()
    {
        $customers = $this->customerService->getCustomers();
        return $this->json($customers);
    }
    
    /**
     * @Route(
     *     "/v1/customers/{id}",
     *     name="customer",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getCustomer($id)
    {
        $customer = $this->customerService->getCustomer($id);
        return $this->json($customer);
    }
    
    /**
     * @Route(
     *     "/v1/customers",
     *     name="saveCustomer",
     *     methods={"POST"}
     * )
     *
     */
    public function saveCustomer(Request $request)
    {
        $customer = json_decode($request->getContent(), true);
        $id = $this->customerService->saveCustomer($customer);
        
        if ($id) {
            return $this->json('Cliente criado com sucesso');
        }
        return false;
    }
}
