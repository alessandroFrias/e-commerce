<?php 

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HelloController {

	/**
	 * @return Response
	 * 
	 * @Route("ola")
	 */
	public function world(){
		return new Response(
			"<html><body>Hello!</body></html>"
		);
	}
}