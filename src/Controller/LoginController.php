<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use \Firebase\JWT\JWT;

class LoginController extends FOSRestController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request)
    {
        $request = json_decode($request->getContent(), true);
        $time = $request['time'] ? $request['time'] * 60 : 60;
        
        $key = "devninja";
        $token = array(
            "iat" => time(),
            "nbf" => time() + $time
        );
        return $this->json(JWT::encode($token, $key, 'HS256'));
    }
}
