<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\ProductService;
use FOS\RestBundle\Controller\FOSRestController;

class ProductController extends FOSRestController
{    
    private $productService;
    
    public function __construct(ProductService $productService){
        $this->productService = $productService;
    }
    
    /**
     * @Route(
     *     "/v1/products",
     *     name="products",
     *     methods={"GET"},
     * )
     *
     * @return JsonResponse
     */
    public function getProducts()
    {
        $produtos = $this->productService->getProducts();
        return $this->json($produtos);
    }
    
    /**
     * @Route(
     *     "/v1/products/{id}",
     *     name="product",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getProduct($id)
    {     
        $produto = $this->productService->getProduct($id);
        return $this->json($produto);
    }
    
    /**
     * @Route(
     *     "/v1/products",
     *     name="saveProduct",
     *     methods={"POST"}
     * )
     *
     * @return JsonResponse
     */
    public function saveProduct(Request $request)
    {
        $produto = json_decode($request->getContent(), true);
        $id = $this->productService->saveProduct($produto);
        
        if ($id) {
            return $this->json('Produto criado com sucesso');
        }
       
    }
}
