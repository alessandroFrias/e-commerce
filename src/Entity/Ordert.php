<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Ordert
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;
    
    /**
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $customer_id;
    
    /**
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $status;
    
    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $created_at;
    /**
     * @return mixed
     */
    
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return number
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * @return number
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param number $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->customer_id = $customer_id;
    }

    /**
     * @param number $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


    
    
}