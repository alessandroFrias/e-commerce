<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $order_id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $product_id;
    
    /**
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $amount;
    
    /**
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price_unit;
    
     /**
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $total;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @return number
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return number
     */
    public function getPriceUnit()
    {
        return $this->price_unit;
    }

    /**
     * @return number
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $order_id
     */
    public function setOrder_id($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProduct_id($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @param number $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param number $price_unit
     */
    public function setPrice_unit($price_unit)
    {
        $this->price_unit = $price_unit;
    }

    /**
     * @param number $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

}