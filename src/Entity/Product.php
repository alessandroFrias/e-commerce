<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     */
    private $id;
    
    /**
     * 
     * @var int
     * 
     * @ORM\Column(type="integer")
     */
    private $sku;
    
    /**
     *
     * @var string
     *
     * @ORM\Column(type="string", length=200)
     */
    private $name;
    
    /**
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;
    
    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $created_at;
    
    /**
     *
     * @ORM\Column(type="datetime")
     */
    private $updated_at;
    
    /**
     * @return int
     */
    public function getId()//: ?int
    {
        return $this->id;
    }

    /**
     * @return number
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return number
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param number $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param number $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

   
}
