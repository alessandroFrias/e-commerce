<?php
namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Customer;
use App\Entity\Ordert;
use App\Entity\Product;
use App\Entity\OrderItem;

class OrderService
{
    private $em;
    private $orderRepository;
    private $orderItemRepository;
    private $productRepository;
    private $customerRepository;
    private $serialize;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;
        $this->orderRepository = $manager->getRepository(Ordert::class);
        $this->orderItemRepository = $manager->getRepository(OrderItem::class);
        $this->productRepository = $manager->getRepository(Product::class);
        $this->customerRepository = $manager->getRepository(Customer::class);
        
    }
    
    public function getOrders(){
        $retorno = [];
        $dados = [];
        
        $orders = $this->orderRepository->findAll();
        
        foreach ($orders as $order){
            $item = array();
            $items = array();
            $orderItems = json_decode($this->orderItemRepository->findOrderId($order->getId()));
            $customer = json_decode($this->customerRepository->findOrderId($order->getCustomerId()));
            
            foreach ($orderItems as $orderItem){
                $product = $this->productRepository->find($orderItem->product_id);
                
                $item = array(
                    'total' => $orderItem->total,
                    'price_unit' => $orderItem->price_unit,
                    'amount' => $orderItem->amount,
                    'product' => array(
                        'id' => $product->getId(),
                        'sku' => $product->getSku(),
                        'title' => $product->getName()
                     )
                );
                
                $items[] = $item;
            }            

            $retorno[] = array(
                'id' => $order->getId(),
                'created_at' => $order->getCreatedAt(),
                'status' => $order->getstatus(),
                'total' => $order->getTotal(),
                'buyer' => array(
                    'id' => $customer->id,
                    'name' => $customer->name,
                    'cpf' => $customer->cpf,
                    'email' => $customer->email
                ),
                'items' => $items
            );
        }
        return $retorno;
    }
    
    public function getOrder($id){
        return $this->orderRepository->find($id);
    }
    
    public function saveOrders($order){
        
        $totalItem = 0;
        $priceItem = 0;
        $idOrder = '';

        $pedido = new Ordert();
        $pedido->setStatus($order['status']);
        $pedido->setCustomerId($order['buyer']['id']);
        $pedido->setCreatedAt(new \DateTime($order['created_at']));
        
        if (is_array($order['items'])) {
            foreach ($order['items'] as $orderItem) {
                $product = $this->productRepository->find($orderItem['product']['id']);
                $totalItem += $product->getPrice() * $orderItem['amount'];
            }
        }
        
        $pedido->setTotal($totalItem);

        $this->em->persist($pedido);
        $this->em->flush();
        $idOrder = $pedido->getId();
        
        //Pedido Item
        
        $pedidoItem = new OrderItem();
        if (is_array($order['items'])) {
            foreach ($order['items'] as $orderItem) {
                $pedidoItem->setOrder_id($idOrder);
                $pedidoItem->setProduct_id($orderItem['product']['id']);
                $pedidoItem->setAmount($orderItem['amount']);
                
                $product = $this->productRepository->find($orderItem['product']['id']);
                $priceItem = $product->getPrice();
                $pedidoItem->setPrice_unit($priceItem);
                $totalItem = $priceItem * $orderItem['amount'];
                $pedidoItem->setTotal($totalItem);
                $this->em->persist($pedidoItem);
                $this->em->flush();
                $this->em->clear();
            }
        }
               
        return $pedido->getId();
    }
    
    public function updateOrders($order){
       $pedido = $this->orderRepository->find($order['order_id']);
       $pedido->setStatus($order['status']);
       $this->em->flush();
       return 'ok';
    }
}

