<?php
namespace App\Service;

use App\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProductService
{
    private $em;
    private $productRepository;
    private $serialize;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;
        $this->productRepository = $manager->getRepository(Product::class);
        
    }
    
    public function getProducts(){
        return $this->productRepository->findAll();
    }
    
    public function getProduct($id){
        return $this->productRepository->find($id);
    }
    
    public function saveProduct($product){
        
        /* Tratativas refrentes aos campos. */
        if(!array_key_exists('sku', $product)){
            throw new HttpException(400, 'sku é um campo obrigatório.');
        }
        
        if(!array_key_exists('name', $product)){
            throw new HttpException(400, 'name é um campo obrigatório.');
        }
        
        if(!array_key_exists('price', $product)){
            throw new HttpException(400,'price é um campo obrigatório.');
        }
        
        if(!array_key_exists('created_at', $product)){
            throw new HttpException(400,'created_at é um campo obrigatório.');
        }
        
        if(!array_key_exists('updated_at', $product)){
            throw new HttpException(400,'updated_at é um campo obrigatório.');
        }
        
        /* Tratativas refrentes aos dados dos campos. */
        
        if($this->productRepository->findProductName($product['name'])){
            throw new HttpException(400,'Já existe um produto com esse nome!');
        }
        
        if($this->productRepository->findProductSku($product['sku'])){
            throw new HttpException(400,'Já existe um produto com esse sku!');
        }
        
        if($product['price'] < 0){
            throw new HttpException(400,'Price deve ser maior que 0!');
        }
        
        $produto = new Product();
        $produto->setSku($product['sku']);
        $produto->setName($product['name']);
        $produto->setPrice($product['price']);
        $produto->setCreatedAt(new \DateTime($product['created_at']));
        $produto->setUpdatedAt(new \DateTime($product['updated_at']));
        
        $this->em->persist($produto);
        $this->em->flush();
        
        return $produto->getId();
            
        
    }
}

