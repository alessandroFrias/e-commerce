<?php
namespace App\Service;

use App\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomerService
{
    private $em;
    private $customerRepository;
    private $serialize;
    
    public function __construct(ObjectManager $manager) {
        $this->em = $manager;
        $this->customerRepository = $manager->getRepository(Customer::class);
        
    }
    
    public function getCustomers(){
        return $this->customerRepository->findAll();
    }
    
    public function getCustomer($id){
        return $this->customerRepository->find($id);
    }
    
    public function saveCustomer($customer){
        
        if(!array_key_exists('name', $customer)){
            throw new HttpException(400, 'name é um campo obrigatório.');
        }
        
        if(!array_key_exists('cpf', $customer)){
            throw new HttpException(400, 'cpf é um campo obrigatório.');
        }
        
        if(!array_key_exists('email', $customer)){
            throw new HttpException(400,'email é um campo obrigatório.');
        }
        
        if(!array_key_exists('created_at', $customer)){
            throw new HttpException(400,'created_at é um campo obrigatório.');
        }
        
        if(!array_key_exists('updated_at', $customer)){
            throw new HttpException(400,'updated_at é um campo obrigatório.');
        }
        
        /* Tratativas refrentes aos dados dos campos. */
        
        if($this->customerRepository->findCustomerCpf($customer['cpf'])){
            throw new HttpException(400,'Já existe um customer com esse cpf!');
        }
        
        if($this->customerRepository->findCustomerEmail($customer['email'])){
            throw new HttpException(400,'Já existe um customer com esse email!');
        }
        
        if($customer['cpf'] && strlen($customer['cpf']) != 11){
            throw new HttpException(400,'Número de caracteres inválidos para o cpf!');
        }
        
        $cliente = new Customer();
        $cliente->setName($customer['name']);
        $cliente->setCpf($customer['cpf']);
        $cliente->setEmail($customer['email']);
        $cliente->setCreatedAt(new \DateTime($customer['created_at']));
        $cliente->setUpdatedAt(new \DateTime($customer['updated_at']));
        
        $this->em->persist($cliente);
        $this->em->flush();
        
        return $cliente->getId();
    }
}

