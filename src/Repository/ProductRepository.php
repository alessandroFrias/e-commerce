<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }
    
//     public function findPriceItem($id){
//         return $this->createQueryBuilder('p')
//                     ->andWhere('p.id = :id')
//                     ->setParameter('val', $id)
//                     ->getQuery()
//                     ->getResult();
//     }

    /**
     * @return Product[] Returns an array of Product objects
    */
    
    public function findProductName($name){
        
        return $this->createQueryBuilder('p')
        ->where("p.name = :val")
        ->setParameter('val', $name)
        ->getQuery()
        ->getResult();
    }
    
    public function findProductSku($sku){
        
        return $this->createQueryBuilder('p')
        ->where("p.sku = :val")
        ->setParameter('val', $sku)
        ->getQuery()
        ->getResult();
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
