<?php

namespace App\Repository;

use App\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Customer::class);
    }
    
    /**
    * @return Customer Returns an Customer objects
    */
    public function findOrderId($id){
        $sql = '
            c.id
            , c.name
            , c.cpf
            , c.email
        ';
        $retorno =  $this->createQueryBuilder('c')
                            ->select($sql)
                            ->andWhere('c.id = :val')
                            ->setParameter('val', $id)
                            ->getQuery()
                            ->getOneOrNullResult();
        return json_encode($retorno);
    }
    
    public function findCustomerCpf($cpf){
        
        return $this->createQueryBuilder('c')
        ->where("c.cpf = :val")
        ->setParameter('val', $cpf)
        ->getQuery()
        ->getResult();
    }
    
    public function findCustomerEmail($email){
        
        return $this->createQueryBuilder('c')
        ->where("c.email = :val")
        ->setParameter('val', $email)
        ->getQuery()
        ->getResult();
    }
}
