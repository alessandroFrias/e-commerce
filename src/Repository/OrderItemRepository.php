<?php

namespace App\Repository;

use App\Entity\OrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderItem[]    findAll()
 * @method OrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OrderItem::class);
    }
    
    public function findOrderId($idOrder){
        $sql = '
            o.total
            , o.price_unit
            , o.amount
            , o.product_id
        ';
        $retorno =  $this->createQueryBuilder('o')
                    ->select($sql)
                    ->andWhere('o.order_id = :val')
                    ->setParameter('val', $idOrder)
                    ->getQuery()
                    ->getResult();
       return json_encode($retorno);
    }
    
    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
     public function findByExampleField($value)
     {
     return $this->createQueryBuilder('p')
     ->andWhere('p.exampleField = :val')
     ->setParameter('val', $value)
     ->orderBy('p.id', 'ASC')
     ->setMaxResults(10)
     ->getQuery()
     ->getResult()
     ;
     }
     */
    
    /*
     public function findOneBySomeField($value): ?Product
     {
     return $this->createQueryBuilder('p')
     ->andWhere('p.exampleField = :val')
     ->setParameter('val', $value)
     ->getQuery()
     ->getOneOrNullResult()
     ;
     }
     */
}
