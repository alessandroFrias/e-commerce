-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 04-Ago-2019 às 23:00
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `symfony`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `customer`
--

INSERT INTO `customer` (`id`, `name`, `cpf`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Maria Aparecida de Souza', '81258705044', 'mariasouza@email.com', '2018-08-27 02:11:43', '2018-08-27 02:30:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ordert`
--

CREATE TABLE `ordert` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `ordert`
--

INSERT INTO `ordert` (`id`, `customer_id`, `total`, `status`, `created_at`) VALUES
(1, 1, '40.00', 'CONCLUDED', '2019-08-01 05:00:00'),
(2, 1, '40.00', 'testeee', '2019-08-01 05:00:00'),
(3, 1, '40.00', 'testeee', '2019-08-01 05:00:00'),
(4, 1, '40.00', 'testeee', '2019-08-01 05:00:00'),
(5, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(6, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(7, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(8, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(9, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(10, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(11, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(12, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(13, 1, '47000.00', 'testeee', '2019-08-01 05:00:00'),
(14, 1, '47000.00', 'Ativo', '2019-08-01 05:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `order_item`
--

INSERT INTO `order_item` (`id`, `amount`, `price_unit`, `total`, `order_id`, `product_id`) VALUES
(1, 1, '19000.00', '19000.00', 6, 2),
(2, 1, '19000.00', '19000.00', 7, 2),
(3, 1, '19000.00', '19000.00', 9, 2),
(4, 1, '19000.00', '19000.00', 10, 2),
(5, 1, '28000.00', '28000.00', 13, 1),
(6, 1, '19000.00', '19000.00', 13, 2),
(7, 1, '28000.00', '28000.00', 14, 1),
(8, 1, '19000.00', '19000.00', 14, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `sku` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `product`
--

INSERT INTO `product` (`id`, `sku`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 898989, 'teste', '28000.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00'),
(2, 654654654, 'Notebook', '19000.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00'),
(3, 8787, 'computador', '800.00', '2019-08-02 05:17:00', '2019-08-01 10:00:00'),
(4, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(5, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(6, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(7, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(8, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(9, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(10, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(11, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(12, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 00:00:00', '2018-08-27 00:00:00'),
(13, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 02:11:43', '2018-08-27 02:11:43'),
(14, 2147483647, 'Casaco Jaqueta Outletdri Inverno Jacquard', '109.90', '2018-08-27 02:11:43', '2018-08-27 02:11:43'),
(15, 2147483647, 'Telefone', '109.90', '2018-08-27 02:11:43', '2018-08-27 02:11:43'),
(16, 2147483647, 'Telefone', '109.90', '2018-08-27 02:11:43', '2018-08-27 02:11:43'),
(17, 2147483647, 'Telefone', '109.90', '2018-08-27 02:11:43', '2018-08-27 02:11:43'),
(18, 1, 'teste', '280.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00'),
(19, 898989, 'Novo', '200.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00'),
(20, 8989879, 'Novo2', '-220.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00'),
(21, 89894879, 'Nov4o2', '-220.00', '2019-08-01 05:00:00', '2019-08-01 10:00:00');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Índices para tabela `ordert`
--
ALTER TABLE `ordert`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `ordert`
--
ALTER TABLE `ordert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
