### Desafio Back - end

---
## Requisitos
1. É necessário que em seu ambiente o PHP esteja instalado, para que possa subir um servidor local.
2. Para desenvolvimento foi utilizando o Xampp para configurar o data-base
3. Na pasta db na raiz do repositório existe um arquivo symfony.sql contendo as tabelas que serão necessárias para realizar os testes. Antes de executar o .sql é necessário criar um database com o nome de symfony.
4. Ter instalado o Postman ou outra ferramenta que realize requisições em determinadas rotas.
5. Utilizar as rotas que foram disponibilizadas no documento do desafio.
